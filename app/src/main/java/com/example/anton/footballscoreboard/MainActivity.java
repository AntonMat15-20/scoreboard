package com.example.anton.footballscoreboard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "StartActivity";

    private Integer counterTeam = 0;
    private Integer counterTeamTwo = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickBtnAddTeam(View view) {
        if (counterTeam < 15) {
            counterTeam++;
        } else {
            counterTeam = 0;
        }
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        counterView.setText(counterTeam.toString());
    }

    public void onClickBtnAddTeamTwo(View view) {
        if (counterTeamTwo < 15) {
            counterTeamTwo++;
        } else {
            counterTeamTwo = 0;
        }
        TextView counterViewTwo = (TextView) findViewById(R.id.txt_counterTwo);
        counterViewTwo.setText(counterTeamTwo.toString());
    }

    public void onClickBtnReset(View view) {
        counterTeam = 0;
        counterTeamTwo = 0;
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        TextView counterViewTwo = (TextView) findViewById(R.id.txt_counterTwo);
        counterView.setText(counterTeam.toString());
        counterViewTwo.setText(counterTeamTwo.toString());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        resetUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counter", counterTeam);
        outState.putInt("counterTwo", counterTeamTwo);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("counter")) {
            counterTeam = savedInstanceState.getInt("counter");
            counterTeamTwo = savedInstanceState.getInt("counterTwo");
        }
        Log.d(TAG, "onRestoreInstanceState");
    }
    private void resetUI() {
        ((TextView) findViewById(R.id.txt_counter)).setText(counterTeam.toString());
        ((TextView) findViewById(R.id.txt_counterTwo)).setText(counterTeamTwo.toString());
        Log.d(TAG, "resetUI");
    }



}
